from django.shortcuts import render

def index(request):
    return render(request, "index2.html")

def about(request):
    return render(request,"about.html")

def contact(request):
    return render(request,"contact.html")

def faq(request):
    return render(request,"faq.html")

def tabel(request):
    return render(request,"tabel.html")
